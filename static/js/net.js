var RespCodes = {
    UserAccepted: 0,
    UsernameTaken: 1,
    SessionFull: 2
}

var Users = []

var Net = {
    init: function () { },

    logIn: async function (username) {
        return await $.ajax({
            type: "POST",
            url: "ADDUSER",
            data: JSON.stringify({ username: username })
        })
    },

    reset: async function () {
        return await $.ajax({
            type: "POST",
            url: "RESET"
        })
    },

    waitForOther: function () {
        let check = function () {
            $.ajax({
                type: "POST",
                url: "WAITING"
            }).then(function (resp) {
                if (resp.users.length == 2) {
                    Users = resp.users
                    UI.startGame()
                } else
                    setTimeout(check, 500)
            })
        }
        check()
    },

    sendMove: async function () {
        return await $.ajax({
            type: "POST",
            url: "MOVE",
            data: JSON.stringify({ board: Game.pieces })
        })
    },

    waitForMove: function () {
        let check = function () {
            $.ajax({
                type: "POST",
                url: "AWAITMOVE",
                data: JSON.stringify({ player: Game.whichPlayer })
            }).then(function (resp) {
                console.log(resp)
                if (resp.accepted) {
                    Game.pieces = resp.board
                    Game.updateBoard()
                    UI.resume()
                    Game.myTurn = true
                } else
                    setTimeout(check, 500)
            })
        }
        check()
    }
}
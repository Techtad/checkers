var Settings = {
    INIT_CAM_POS: {
        X: 0,
        Y: 550,
        Z: 600
    }
}

var TextureLoader = new THREE.TextureLoader()

var Materials = {
    Wood_White: new THREE.MeshBasicMaterial({
        map: TextureLoader.load("img/wood.png"),
        color: 0xffffff
    }),

    Wood_Beige: new THREE.MeshBasicMaterial({
        map: TextureLoader.load("img/wood.png"),
        color: 0xe4e4cb
    }),

    Wood_Black: new THREE.MeshBasicMaterial({
        map: TextureLoader.load("img/wood.png"),
        color: 0x000000
    }),

    Wood_Red: new THREE.MeshBasicMaterial({
        map: TextureLoader.load("img/wood.png"),
        color: 0xff0000
    }),

    Piece_Highlight: new THREE.MeshBasicMaterial({
        map: TextureLoader.load("img/wood.png"),
        color: 0xffff00
    }),

    Piece_Selected: new THREE.MeshBasicMaterial({
        map: TextureLoader.load("img/wood.png"),
        color: 0x00ff00
    }),

    Move_Highlight: new THREE.MeshBasicMaterial({
        map: TextureLoader.load("img/wood.png"),
        color: 0x0000ff
    })
}

var Pieces = {
    None: 0,
    White: 1,
    Black: 2
}

var Players = {
    White: 0,
    Black: 1
}

var Game = {
    init: function () {
        this.whichPlayer = -1
        this.paused = true

        this.scene = new THREE.Scene()

        this.renderer = new THREE.WebGLRenderer({ antialias: true })
        this.renderer.setClearColor(0xeeeeee)
        this.renderer.setSize($(window).width(), $(window).height())

        this.camera = new THREE.PerspectiveCamera(45, $(window).width() / $(window).height(), 1, 10000)
        this.camera.position.set(Settings.INIT_CAM_POS.X, Settings.INIT_CAM_POS.Y, Settings.INIT_CAM_POS.Z)
        this.camera.lookAt(this.scene.position)
        this.camera.updateProjectionMatrix()

        window.addEventListener("resize", (e) => { Game.renderer.setSize($(window).width(), $(window).height()); Game.camera.aspect = $(window).width() / $(window).height(); Game.camera.updateProjectionMatrix(); })

        this.orbitControls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
        this.orbitControls.enabled = false

        this.clock = new THREE.Clock()

        this.createScene()

        $("#root").append(this.renderer.domElement)

        this.selectedPiece = null
        this.highlightables = []
        this.highlighted = null

        this.myTurn = false

        $(document).mousemove(function (event) {
            if (!Game.myTurn) return

            let raycaster = new THREE.Raycaster()
            let mouseVector = new THREE.Vector2((event.clientX / $(window).width()) * 2 - 1, -(event.clientY / $(window).height()) * 2 + 1)
            raycaster.setFromCamera(mouseVector, Game.camera)
            let intersects = raycaster.intersectObjects(Game.highlightables);
            if (Game.highlighted && Game.highlighted != Game.selectedPiece) Game.highlighted.unHighlight()
            if (intersects.length > 0) {
                Game.highlighted = intersects[0].object
                if (Game.highlighted != Game.selectedPiece) Game.highlighted.highlight()
            }
        })
        $(document).mousedown(function (event) {
            if (!Game.myTurn) return

            let raycaster = new THREE.Raycaster()
            let mouseVector = new THREE.Vector2((event.clientX / $(window).width()) * 2 - 1, -(event.clientY / $(window).height()) * 2 + 1)
            raycaster.setFromCamera(mouseVector, Game.camera)
            let intersects = raycaster.intersectObjects(Game.highlightables.concat(Game.positions));
            if (intersects.length > 0) {
                if (intersects[0].object.name == "POSITION") {
                    if (Game.selectedPiece) {
                        let pX = Game.selectedPiece.x
                        let pZ = Game.selectedPiece.z
                        let nX = intersects[0].object.posX
                        let nZ = intersects[0].object.posZ
                        if (!Game.isMoveValid(pX, pZ, nX, nZ, Game.selectedPiece.player == Players.Black)) return

                        Game.pieces[nX][nZ] = Game.selectedPiece.player == Players.White ? Pieces.White : Pieces.Black
                        Game.pieces[pX][pZ] = Pieces.None
                        Game.updateBoard()

                        Game.selectedPiece.unHighlight()
                        Game.clearMoveHighlight()
                        Game.selectedPiece = null

                        Net.sendMove().then(function () {
                            UI.waitingScreen("Czekanie na ruch drugiego gracza...")
                            Net.waitForMove()
                            Game.myTurn = false
                        })
                    }
                } else if (intersects[0].object.name == "PIECE" && intersects[0].object != Game.selectedPiece) {
                    if (Game.selectedPiece) {
                        Game.selectedPiece.unHighlight()
                        Game.clearMoveHighlight()
                    }
                    Game.selectedPiece = intersects[0].object
                    Game.selectedPiece.material = Materials.Piece_Selected
                    Game.highlightMoves(Game.selectedPiece.x, Game.selectedPiece.z, Game.selectedPiece.player == Players.Black)
                }
            }
        })
    },

    createScene: function () {
        //this.axes = new THREE.AxesHelper(5000)
        //this.scene.add(this.axes)

        //this.sun = new THREE.DirectionalLight(0xffffff, 2)
        //this.scene.add(this.sun)

        this.positions = []
        this.highlightedPos = []
        this.board = new THREE.Object3D()
        let blockGeom = new THREE.BoxGeometry(50, 25, 50)
        for (let x = 0; x < 8; x++) {
            for (let z = 0; z < 8; z++) {
                let whiteBlock = x % 2 == z % 2
                let mat = whiteBlock ? Materials.Wood_Beige : Materials.Wood_Red
                let block = new THREE.Mesh(blockGeom, mat)
                block.position.set((x - 3.5) * 50, -12.5, (z - 3.5) * 50)
                block.name = "POSITION"
                block.posX = x
                block.posZ = z
                this.board.add(block)
                if (!whiteBlock) this.positions.push(block)
            }
        }
        this.scene.add(this.board)
    },

    start: function () {
        let cameraDirection = this.whichPlayer == Players.White ? 1 : -1
        this.camera.position.z *= cameraDirection
        this.camera.lookAt(Game.scene.position)
        this.addPieces()
        this.orbitControls.enabled = true

        if (this.whichPlayer == Players.Black) {
            UI.waitingScreen("Czekanie na ruch drugiego gracza...")
            Net.waitForMove()
        } else
            this.myTurn = true
    },

    addPieces: function () {
        this.pieces = []
        for (let x = 0; x < 8; x++) {
            this.pieces[x] = []
            for (let z = 0; z < 8; z++) {
                if (z < 3 && x % 2 != z % 2)
                    this.pieces[x][z] = Pieces.Black
                else if (z > 4 && x % 2 != z % 2)
                    this.pieces[x][z] = Pieces.White
                else
                    this.pieces[x][z] = Pieces.None
            }
        }

        this.whitePieces = []
        this.blackPieces = []
        for (let x = 0; x < 8; x++) {
            for (let z = 0; z < 8; z++) {
                if (this.pieces[x][z] == Pieces.White || this.pieces[x][z] == Pieces.Black) {
                    let blackPiece = this.pieces[x][z] == Pieces.Black
                    let obj = new Piece(x, z, blackPiece)
                    obj.position.set((x - 3.5) * 50, 0, (z - 3.5) * 50)

                    if (blackPiece) this.blackPieces.push(obj)
                    else this.whitePieces.push(obj)

                    this.scene.add(obj)
                    if (blackPiece == (this.whichPlayer == Players.Black))
                        this.highlightables.push(obj)
                }
            }
        }
    },

    updateBoard: function () {
        let whiteIndex = 0
        let blackIndex = 0
        for (let x = 0; x < 8; x++) {
            for (let z = 0; z < 8; z++) {
                let pieceObj
                if (this.pieces[x][z] == Pieces.White)
                    pieceObj = this.whitePieces[whiteIndex++]
                else if (this.pieces[x][z] == Pieces.Black)
                    pieceObj = this.blackPieces[blackIndex++]
                else continue

                pieceObj.position.set((x - 3.5) * 50, 0, (z - 3.5) * 50)
                pieceObj.x = x
                pieceObj.z = z
            }
        }
    },

    isMoveValid(oldX, oldZ, newX, newZ, black) {
        return (this.pieces[newX][newZ] == Pieces.None && Math.abs(newX - oldX) == 1 && (newZ - oldZ == (black ? 1 : -1)))
    },

    highlightMoves(x, z, black) {
        for (let xOffset = -1; xOffset <= 1; xOffset += 2) {
            for (let zOffset = -1; zOffset <= 1; zOffset += 2) {
                let checkedX = x + xOffset
                let checkedZ = z + zOffset
                if (checkedX >= 0 && checkedX < this.pieces.length && checkedZ >= 0 && checkedZ < this.pieces.length && this.isMoveValid(x, z, checkedX, checkedZ, black)) {
                    for (let pos of this.positions) {
                        if (pos.posX == checkedX && pos.posZ == checkedZ) {
                            pos.material = Materials.Move_Highlight
                            this.highlightedPos.push(pos)
                        }
                    }
                }
            }
        }
    },

    clearMoveHighlight() {
        for (let pos of this.highlightedPos) pos.material = Materials.Wood_Red
        this.highlightedPos = []
    },

    update: function () {
        let delta = this.clock.getDelta()

        this.render()
    },

    render: function () {
        this.renderer.render(this.scene, this.camera)
        if (!this.paused)
            requestAnimationFrame(this.update.bind(this))
    },

    pause: function () {
        this.paused = true
    },

    resume: function () {
        this.paused = false
        requestAnimationFrame(this.update.bind(this))
    }
}
var Program = {
    init: function () {
        Net.init()
        UI.init()
        Game.init()
    }
}

window.addEventListener("DOMContentLoaded", function (event) {
    Program.init()
    Game.resume()
})
var http = require("http")
var fs = require("fs")

function sendFile(fileName, res) {
    let directory = "static"
    if (fileName == "/") fileName = "/index.html"
    if (fileName.includes("/libs")) directory = __dirname
    fs.readFile(`${directory}${fileName}`, function (error, data) {
        if (error) {
            console.log(error)
            res.writeHead(404, { "Content-Type": "text/html;charset=utf-8" })
            res.end(`Błąd 404: Nie znaleziono pliku ./static${fileName}`)
        } else {
            let ext = fileName.split(".")[1]
            let contentType = "text/html;charset=utf-8"
            switch (ext) {
                case "js":
                    contentType = "application/javascript"
                    break
                case "css":
                    contentType = "text/css"
                    break
                case "jpg":
                    contentType = "image/jpeg"
                    break
                case "mp3":
                    contentType = "audio/mpeg"
                    break
                case "png":
                    contentType = "image/png"
                    break
                case "svg":
                    contentType = "image/svg+xml"
                    break
                case "gif":
                    contentType = "image/gif"
                    break
                default: break
            }
            res.writeHead(200, { "Content-Type": contentType })
            res.end(data)
        }
    })
}

var Users = []
var Board
var WhoseTurn = 3

function createBoard() {
    Board = []
    for (let x = 0; x < 8; x++) {
        Board[x] = []
        for (let z = 0; z < 8; z++) {
            if (z < 3 && x % 2 != z % 2)
                Board[x][z] = 2
            else if (z > 4 && x % 2 != z % 2)
                Board[x][z] = 1
            else
                Board[x][z] = 0
        }
    }
}

function servResponse(req, res) {
    let allData = ""
    req.on("data", function (data) {
        allData += data
    })
    req.on("end", function (data) {
        console.log("Data(" + req.url + "):" + allData)
        let obj
        if (allData) {
            try {
                obj = JSON.parse(allData)
            } catch (error) {
                console.log(error)
                res.writeHead(400, { "Content-Type": "text/html;charset=utf-8" })
                res.end(error)
            }
        }
        switch (req.url) {
            case '/ADDUSER':
                if (obj.username && obj.username.toString()) {
                    if (Users.length < 2) {
                        if (Users.includes(obj.username.toString())) {
                            res.writeHead(201, { "Content-Type": "application/json;charset=utf-8" })
                            res.end(JSON.stringify({ code: 1 }))
                        } else {
                            Users.push(obj.username.toString())
                            let index = Users.indexOf(obj.username.toString())
                            if (index == 1) {
                                createBoard()
                                WhoseTurn = 0
                            }
                            res.writeHead(202, { "Content-Type": "application/json;charset=utf-8" })
                            res.end(JSON.stringify({ code: 0, username: Users[index], index: index }))
                        }
                    } else {
                        res.writeHead(200, { "Content-Type": "application/json;charset=utf-8" })
                        res.end(JSON.stringify({ code: 2 }))
                    }
                } else {
                    res.writeHead(400, { "Content-Type": "text/html;charset=utf-8" })
                    res.end("Nieprawidłowe dane")
                }
                break
            case '/RESET':
                Users = []
                createBoard()
                res.writeHead(200, { "Content-Type": "application/json;charset=utf-8" })
                res.end(JSON.stringify({ reset: true }))
                break
            case '/WAITING':
                res.writeHead(200, { "Content-Type": "application/json;charset=utf-8" })
                res.end(JSON.stringify({ users: Users }))
                break
            case '/MOVE':
                Board = obj.board
                WhoseTurn = Math.abs(WhoseTurn - 1)
                res.writeHead(200, { "Content-Type": "application/json;charset=utf-8" })
                res.end(JSON.stringify({ accepted: true }))
                break
            case '/AWAITMOVE':
                res.writeHead(200, { "Content-Type": "application/json;charset=utf-8" })
                if (obj.player == WhoseTurn) {
                    res.end(JSON.stringify({ accepted: true, board: Board }))
                }
                else
                    res.end(JSON.stringify({ accepted: false }))
                break
            default:
                console.log("Błędny POST: " + req.url)
                res.writeHead(400, { "Content-Type": "application/json;charset=utf-8" })
                res.end("Błędna akcja POST: " + req.url)
                break
        }
    })
}

var server = http.createServer(function (req, res) {
    console.log(req.method + " : " + req.url)
    switch (req.method) {
        case "GET":
            sendFile(decodeURI(req.url).split('?')[0], res)
            break;
        case "POST":
            servResponse(req, res)
            break;
    }
})

const port = 3000
server.listen(3000, function () {
    console.log("Start serwera na porcie " + port)
})
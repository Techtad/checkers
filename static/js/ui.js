var UI = {
    init: function () {
        this.buttons()
    },

    buttons: function () {
        $("#btn-login").on("click", function (event) {
            if ($("#input-username").val().length > 0) {
                Net.logIn($("#input-username").val()).then((resp) => {
                    switch (resp.code) {
                        case RespCodes.UserAccepted:
                            Game.whichPlayer = resp.index
                            UI.waitingScreen("Czekanie do dołączenie drugiego gracza...")
                            Net.waitForOther()
                            break
                        case RespCodes.UsernameTaken:
                            $("#status").text("Nick zajęty")
                            break
                        case RespCodes.SessionFull:
                            $("#status").text("Jest już dwóch graczy")
                            break
                    }
                })
            } else {
                $("#status").text("Nick nie może być pusty")
            }
        })

        $("#btn-reset").on("click", function (event) {
            Net.reset().then((resp) => {
                if (resp.reset)
                    $("#status").text("Zresetowano serwer")
                else
                    $("#status").text("Błąd podczas resetowania serwera")
            })
        })
    },

    waitingScreen: function (text) {
        $("#ui").css("background-color", "rgba(0, 0, 0, 0.25)")
        $("#ui").css("width", "100%")
        $("#ui").css("height", "100%")
        $("#login-form").css("display", "none")
        $("#waiting-screen").css("display", "block")
        $("#status").html(text)
    },

    startGame: function () {
        $("#waiting-screen").css("display", "none")
        $("#status").html(`Witaj <i>${Users[Game.whichPlayer]}</i>, grasz ${Game.whichPlayer == 0 ? '<w>białymi</w>' : '<b>czarnymi</b>'}, przeciwko <e>${Users[Math.abs(Game.whichPlayer - 1)]}</e>`)
        $("#ui").css("background-color", "rgba(0,0,0,0)")
        $("#ui").css("width", "0")
        $("#ui").css("height", "0")

        Game.start()
    },

    resume: function () {
        $("#waiting-screen").css("display", "none")
        $("#ui").css("background-color", "rgba(0,0,0,0)")
        $("#ui").css("width", "0")
        $("#ui").css("height", "0")
        $("#status").html(`Twój ruch, <i>${Users[Game.whichPlayer]}</i>, grasz ${Game.whichPlayer == 0 ? '<w>białymi</w>' : '<b>czarnymi</b>'}, przeciwko <e>${Users[Math.abs(Game.whichPlayer - 1)]}</e>`)
    },
}
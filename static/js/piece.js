class Piece extends THREE.Mesh {
    constructor(x, z, isBlack) {
        let geom = new THREE.CylinderGeometry(24, 24, 25, 16)
        let mat = isBlack ? Materials.Wood_Black : Materials.Wood_White
        super(geom, mat)

        this.x = x
        this.z = z

        this.normalMat = mat
        this.highlightMat = Materials.Piece_Highlight
        this.name = "PIECE"
        this.player = isBlack ? Players.Black : Players.White
    }

    highlight() {
        this.material = this.highlightMat
    }

    unHighlight() {
        this.material = this.normalMat
    }
}